/**
 * Copyright 2015 Atsushi Kojo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

const ftp = require("ftp")
const fs = require("fs")

module.exports=RED=>{
    "use strict"
    function FtpNode(n) {
        RED.nodes.createNode(this, n)
        const {
            credentials={},
        }=this
        const {
            host="localhost",
            port=21,
            secure=false,
            connTimeout=10000,
            pasvTimeout=10000,
            keepalive=10000,
            rejectUnauthorized,
            acceptSHA256,
        }=n
        const secureOptions={
            rejectUnauthorized,
        }
        if(acceptSHA256) secureOptions.checkServerIdentity=(_, c)=>
            c.fingerprint256===acceptSHA256?undefined:new Error(`Cert ${c.fingerprint256} doesn't match required (${acceptSHA256})`)
        const {
            user="anonymous",
            password="anonymous@",
        }=credentials
        this.options = {
            host,
            port,
            secure,
            secureOptions,
            user,
            password,
            connTimeout,
            pasvTimeout,
            keepalive,
        }
    }

    RED.nodes.registerType("ftp", FtpNode, {
        credentials: {
            user: {
                type: "text",
            },
            password: {
                type: "password",
            },
        }
    })

    function FtpInNode(n) {
        RED.nodes.createNode(this, n)
        const {operation}=n
        const ftpConfig=RED.nodes.getNode(n.ftp)

        if (!ftpConfig) {
            this.error("missing ftp configuration")
            return
        }

        this.connectionsPool=[]

        this.on("close", ()=>{
            if(this.connectionsPool && this.connectionsPool.length){
                this.connectionsPool
                    .filter(conn=>conn&&conn.end)
                    .forEach(conn=>{
                        try {
                            conn.end()
                        }catch(e){
                            this.error(`Cannot end conn from pool: ${e.message}`)
                        }
                    })
            }
        })
        this.on("input", msg=>{
            let {
                filename=n.filename,
                localFilename=n.localFilename,
            }=msg
            const conn = new ftp()
            this.connectionsPool.push(conn)
            const connectionTracker=new Promise((resolve, reject)=>{
                const definePromise = (err, result)=>{
                    if (err) return reject(err)
                    let progress
                    if(operation=="get"){
                        progress=new Promise(resolve=>result.once("close", ()=>resolve()))
                        result.pipe(fs.createWriteStream(localFilename))
                    }else{
                        progress=Promise.resolve(result)
                    }
                    progress
                        .then(r=>resolve(r))
                        .catch(e=>reject(e))
                }
                conn.on("ready", ()=>{
                    switch (operation) {
                    case "list":
                        conn.list(definePromise)
                        break
                    case "get":
                        conn.get(filename, definePromise)
                        break
                    case "put":
                        conn.put(localFilename, filename, definePromise)
                        break
                    case "delete":
                        conn.delete(filename, definePromise)
                        break
                    case "mkdir":
                        conn.mkdir(filename, true, definePromise)
                        break
                    }
                })
                conn.on("error", err=>reject(err))
                //Clone object (object spread not present on node 10)
                const options=Object.assign({}, ftpConfig.options)
                const {auth}=msg
                if(auth){
                    options.user=auth.username
                    options.password=auth.password
                }
                conn.connect(options)
            })
            connectionTracker
                .then(res=>this.send(Object.assign(msg, {
                    payload: res || `${operation} successful`,
                    filename,
                    localFilename,
                })))
                .then(()=>this.status({}))
                .catch(e=>{
                    if(e) this.error(e)
                    this.status({fill: "red", shape: "ring", text: `failed ${e.message}`})
                })
                .finally(()=>{
                    conn.end() // Errors are uncaught (if any)
                    let idx=this.connectionsPool.findIndex(c=>c===conn)
                    if(idx>=0) this.connectionsPool.splice(idx, 1)
                })
        })
    }
    RED.nodes.registerType("ftp in", FtpInNode)
}
